![Scheme](./vlans.png)

| Device | Interface | IP        | Mask            | Default Gateway |
|--------|-----------|-----------|-----------------|-----------------|
| PC1    | Fa0/1     | 10.0.10.2 | 255.255.255.0   | 10.0.10.1       |
| PC2    | Fa0/1     | 10.0.20.2 | 255.255.255.0   | 10.0.20.1       |
| PC3    | Fa0/1     | 10.0.30.2 | 255.255.255.0   | 10.0.30.1       |
| PC4    | Fa0/1     | 10.0.40.2 | 255.255.255.0   | 10.0.40.1       |
| PC5    | Fa0/1     | 10.0.50.2 | 255.255.255.0   | 10.0.50.1       |
| S1     | VLAN 10   | 10.0.10.1 | 255.255.255.0   |                 |
| S1     | VLAN 20   | 10.0.20.1 | 255.255.255.0   |                 |
| S1     | VLAN 30   | 10.0.30.1 | 255.255.255.0   |                 |
| S1     | G0/1      | 10.0.0.1  | 255.255.255.252 |                 |
| R1     | G0/1      | 10.0.0.2  | 255.255.255.252 |                 |
| R1     | G0/1.40   | 10.0.40.1 | 255.255.255.0   |                 |
| R1     | G0/1.50   | 10.0.50.1 | 255.255.255.0   |                 |


Необходимо назначить IP адреса компьютерам PC1-PC5.

Конфигурируем S1:
```
Switch>en
Switch#conf t
Switch(config)#hostname S1

#Создадим vlanы и назначим их на порты
S1(config)#vlan 10
S1(config-vlan)#vlan 20
S1(config-vlan)#vlan 30
S1(config-vlan)#exit
S1(config)#int vlan 10
S1(config-if)#ip address 10.0.10.1 255.255.255.0
S1(config-if)#int vlan 20
S1(config-if)#ip address 10.0.20.1 255.255.255.0
S1(config-if)#int vlan 30
S1(config-if)#ip address 10.0.30.1 255.255.255.0
S1(config-if)#int fa0/1
S1(config-if)#switchport mode access
S1(config-if)#switchport access vlan 10
S1(config-if)#int fa0/2
S1(config-if)#switchport mode access
S1(config-if)#switchport access vlan 20
S1(config-if)#int fa0/3
S1(config-if)#switchport mode access
S1(config-if)#switchport access vlan 30
S1(config-if)#exit

#Включим маршрутизацию на коммутаторе
S1(config)#ip routing
#Теперь PC1-PC3 могут пинговать друг друга

#Потушим ненужные порты
S1(config-if)#int range fa0/4-24
S1(config-if-range)#sh
S1(config-if-range)#int g0/2
S1(config-if-range)#sh

#Настроим RIP
S1(config)#int g0/1
S1(config-if)#no switchport
S1(config-if)#ip add 10.0.0.1 255.255.255.252
S1(config-if)#exit

S1(config-if)#router rip
S1(config-router)#version 2
S1(config-router)#no auto-summary
S1(config-router)#passive-interface fa0/1
S1(config-router)#passive-interface fa0/2
S1(config-router)#passive-interface fa0/3
S1(config-router)#network 10.0.10.0
S1(config-router)#network 10.0.20.0
S1(config-router)#network 10.0.30.0
S1(config-router)#end

S1#write memory
```

Конфигурируем S2

```
Switch>en
Switch#conf t
Switch(config)#hostname S2

#Создадим vlanы и назначим их на порты
S2(config)#vlan 40
S2(config-vlan)#vlan 50
S2(config-vlan)#int fa0/1
S2(config-if)#switchport mode access
S2(config-if)#switchport access vlan 40
S2(config-if)#int fa0/2
S2(config-if)#switchport mode access
S2(config-if)#switchport access vlan 50
S2(config-if)#int g0/1
S2(config-if)#switchport mode trunk
S2(config-if)#switchport trunk allowed vlan 40,50

#Потушим ненужные порты
S1(config-if)#int range fa0/3-24
S1(config-if-range)#sh
S1(config-if-range)#int g0/2
S1(config-if-range)#sh
S2(config-if)#end

S2#wr mem
```
Конфигурируем R1
```
Router>en
Router#conf t
Router(config)#hostname R1

#Настроим сабинтерфейсы
R1(config)#int g0/0.40
R1(config-subif)#encapsulation dot1Q 40
R1(config-subif)#ip add 10.0.40.1 255.255.255.0
R1(config-subif)#int g0/0.50
R1(config-subif)#encapsulation dot1Q 50
R1(config-subif)#ip add 10.0.50.1 255.255.255.0
R1(config-subif)#int g0/0
R1(config-if)#no sh
R1(config-if)#exit

#Теперь PC4 и PC5 могут пинговать друг друга

#Настроим RIP на роутере
R1(config)#router rip
R1(config-router)#version 2
R1(config-router)#no auto-summary
R1(config-router)#passive-interface g0/0
R1(config-router)#network 10.0.40.0
R1(config-router)#network 10.0.50.0
R1(config-router)#int g0/1
R1(config-if)#ip add 10.0.0.2 255.255.255.0
R1(config-if)#no sh
R1(config-if)#end

R1#wr mem
```

Должно все работать)


